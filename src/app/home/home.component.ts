import {Component, OnInit,ViewEncapsulation} from '@angular/core';
import {SelectItem} from 'primeng/primeng';
import * as $ from 'jquery';



@Component({
  selector: 'home',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [ './home.component.css' ],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit{
  calendarOptions:Object;
  months: SelectItem[];
  selectedMonth:string;
  selectedYear:number;
  display:boolean=false;

  constructor(){
    this.months=[];
    this.months.push({label:'January', value:{id:1, name: 'January'}});
    this.months.push({label:'February', value:{id:2, name: 'February'}});
    this.months.push({label:'March', value:{id:3, name: 'March'}});
    this.months.push({label:'April', value:{id:4, name: 'April'}});
    this.months.push({label:'May', value:{id:5, name: 'May'}});
    this.months.push({label:'June', value:{id:6, name: 'June'}});
    this.months.push({label:'July', value:{id:7, name: 'July'}});
    this.months.push({label:'August', value:{id:8, name: 'August'}});
    this.months.push({label:'September', value:{id:9, name: 'September'}});
    this.months.push({label:'October', value:{id:10, name: 'October'}});
    this.months.push({label:'November', value:{id:11, name: 'November'}});
    this.months.push({label:'December', value:{id:12, name: 'December'}});

  }
  ngOnInit(){
    /*setting otions to calendar*/
    this.calendarOptions= {
      /*use arrow functions, to be able to use "this"*/
      dayClick: (date)=>{
        let a =date.format("MMM Do YY");
        $('.info').text(a);
        this.display=true;
      },
      fixedWeekCount : false,
      height:410,
      weekNumberCalculation:'ISO',
      editable: true,
      eventLimit: true,
      header:{
        left:   'title',
        right:  'today'

      }
    };

  }
  createCalendar( year:number, month:number,id:string="test1") {
    if(month===undefined || month==null)month=1;
    if(year===undefined || year==null)alert("input year")
    else{
      /*manipulating with div's*/
      $('#calendar').fullCalendar('gotoDate', year.toString() + '-' + month + '-02');
      let existingdiv1 = document.getElementById("calendar");
      $('#' + id).append(existingdiv1);}
  }

  /* move calendar to default div*/
  default(){
    let existingdiv1 = document.getElementById("calendar");
    $('.default').append(existingdiv1);

  }


}




